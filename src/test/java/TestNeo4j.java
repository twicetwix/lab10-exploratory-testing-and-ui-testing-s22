import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import static java.lang.Thread.sleep;

public class TestNeo4j {

    @Test
    public void testNeo4SearchOnIndexArticles() throws InterruptedException {
        WebDriver driver = new FirefoxDriver();
        driver.get("https://neo4j.com/");
        System.out.println("Page Title is " + driver.getTitle());

        WebElement searchIcon = driver.findElement(By.cssSelector("span[class='n-icon n-icon-search']"));
        searchIcon.click();
        sleep(1000);

        WebElement searchInput = driver.findElement(By.cssSelector("input[placeholder='Search for docs, blogs, resources, etc']"));
        searchInput.sendKeys("Index");
        sleep(5000);

        WebElement results = driver.findElement(By.id("result-items-container"));
        WebElement firstResult = results.findElement(By.xpath("./div/a/div/div/div[2]/span"));

        Assert.assertEquals("Neo4j indexes", firstResult.getText());
        sleep(4000);
        firstResult.click();
        sleep(4000);

        WebElement articleHeaderLevel1 = driver.findElement(By.cssSelector(".doc > h1"));
        Assert.assertEquals("Neo4j Versioning", articleHeaderLevel1.getText());
        driver.quit();
    }
}
